package com.hk.platform.commons.tree;

import java.util.List;
import java.util.Map;

import com.hk.commons.util.CollectionUtils;

/**
 * @author kevin
 * @date 2018-08-29 08:55
 */
public interface TreeGenerator<T extends TreeNode<?>> {

    /**
     * 获取根节点
     *
     * @param params params
     * @return 根节点
     */
    List<T> rootNode(Map<String, Object> params);

    /**
     * 获取上级节点的子节点
     *
     * @param parentId 上级节点Id
     * @param params   参数
     * @return 子节点
     */
    List<T> childList(String parentId, Map<String, Object> params);

    /**
     * 当前节点是否为根节点
     *
     * @param t      当前节点
     * @param params 参数
     * @return true or false
     */
    default boolean isRootNode(T t, Map<String, Object> params) {
        return CollectionUtils.isEmpty(childList(t.getId(), params));
    }
}
