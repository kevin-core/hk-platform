package com.hk.pms.service;


import com.hk.core.cache.service.JdbcCacheService;
import com.hk.core.service.jdbc.JdbcBaseService;
import com.hk.pms.domain.SysOrg;

/**
 * @author kevin
 * @date 2018-04-12 16:51
 */
public interface SysOrgService extends JdbcCacheService<SysOrg, String> {

}
