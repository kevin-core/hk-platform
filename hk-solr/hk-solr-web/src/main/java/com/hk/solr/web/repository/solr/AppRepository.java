package com.hk.solr.web.repository.solr;

import com.hk.core.solr.respoitory.BaseSolrRepository;
import com.hk.solr.web.domain.App;

/**
 * @author sjq-278
 * @date 2018-12-03 11:44
 */
public interface AppRepository extends BaseSolrRepository<App, String> {

}
