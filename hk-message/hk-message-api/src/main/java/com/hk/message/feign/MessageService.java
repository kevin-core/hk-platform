package com.hk.message.feign;

interface MessageService {

    String SERVICE_NAME = "hk-message";

    String CONTEXT_PATH = "/message";

}
