package com.hk.pms.repository.jdbc;


import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.pms.domain.SysOrg;

/**
 * @author kevin
 * @date 2018-04-12 16:38
 */
public interface SysOrgRepository extends StringIdJdbcRepository<SysOrg> {
}
