package com.hk.pms.repository.jdbc;

import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.pms.domain.SysPermissionResource;

/**
 * @author kevin
 * @date 2018-08-28 16:37
 */
public interface SysPermissionResourceRepository extends StringIdJdbcRepository<SysPermissionResource> {
}
