package com.hk.pms.repository.jdbc;

import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.pms.domain.SysConfig;

/**
 * @author kevin
 * @date 2018-09-20 15:20
 */
public interface SysConfigRepository extends StringIdJdbcRepository<SysConfig> {
}
