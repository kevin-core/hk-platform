package com.hk.solr.api.feign;

/**
 * @author sjq-278
 * @date 2018-12-04 16:46
 */
interface SolrService {

    String SERVICE_NAME = "hk-solr-web";

    String CONTEXT_PATH = "/";
}
