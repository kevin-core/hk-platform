package com.hk.oauth2.server.repository.jdbc;

import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.oauth2.server.entity.SysRole;

/**
 * @author kevin
 * @date 2018-08-03 08:54
 */
public interface RoleRepository extends StringIdJdbcRepository<SysRole> {
}
