package com.hk.oauth2.server.service;

import com.hk.core.cache.service.JdbcCacheService;
import com.hk.oauth2.server.entity.SysOrgDept;

/**
 * @author kevin
 * @date 2018-10-25 15:15
 */
public interface SysOrgDeptService extends JdbcCacheService<SysOrgDept, String> {

}
