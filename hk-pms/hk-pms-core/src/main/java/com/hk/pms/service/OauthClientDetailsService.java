package com.hk.pms.service;

import com.hk.core.service.jdbc.JdbcBaseService;
import com.hk.pms.domain.OauthClientDetails;

/**
 * @author huangkai
 * @date 2019-01-03 16:56
 */
public interface OauthClientDetailsService extends JdbcBaseService<OauthClientDetails, String> {

}
