package com.hk.pms.repository.jdbc;

import com.hk.core.data.jdbc.repository.JdbcRepository;
import com.hk.pms.domain.OauthClientDetails;

/**
 * @author huangkai
 * @date 2019-01-03 16:55
 */
public interface OauthClientDetailsRepository extends JdbcRepository<OauthClientDetails, String> {
}
