package com.hk.solr.web.repository.solr;

import com.hk.core.solr.respoitory.BaseSolrRepository;
import com.hk.solr.web.domain.Commodity;

/**
 * @author huangkai
 * @date 2018-12-2 21:43
 */
public interface CommodityRepository extends BaseSolrRepository<Commodity, String> {

}
