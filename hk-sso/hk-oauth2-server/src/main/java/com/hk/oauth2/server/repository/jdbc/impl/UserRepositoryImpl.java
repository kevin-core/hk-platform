package com.hk.oauth2.server.repository.jdbc.impl;

import com.hk.core.data.jdbc.JdbcDaoSupport;
import com.hk.oauth2.server.repository.jdbc.custom.CustomUserRepository;

/**
 * @author kevin
 * @date 2018-10-15 16:33
 */
public class UserRepositoryImpl extends JdbcDaoSupport implements CustomUserRepository {

}
