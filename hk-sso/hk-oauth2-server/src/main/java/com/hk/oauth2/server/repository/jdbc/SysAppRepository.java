package com.hk.oauth2.server.repository.jdbc;

import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.oauth2.server.entity.SysApp;

/**
 * @author kevin
 * @date 2018-08-02 16:53
 */
public interface SysAppRepository extends StringIdJdbcRepository<SysApp> {


}
