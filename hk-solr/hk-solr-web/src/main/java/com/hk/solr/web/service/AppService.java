package com.hk.solr.web.service;

import com.hk.solr.web.domain.App;

/**
 * @author sjq-278
 * @date 2018-12-03 11:44
 */
public interface AppService extends BaseSolrService<App, String> {


}
