package com.hk.pms.service;

import com.hk.core.service.jdbc.JdbcBaseService;
import com.hk.pms.domain.SysConfig;

/**
 * @author kevin
 * @date 2018-09-20 20:06
 */
public interface SysConfigService extends JdbcBaseService<SysConfig, String> {
	
}
