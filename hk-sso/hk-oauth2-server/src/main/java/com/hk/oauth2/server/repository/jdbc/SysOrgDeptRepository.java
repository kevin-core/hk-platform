package com.hk.oauth2.server.repository.jdbc;

import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.oauth2.server.entity.SysOrgDept;

/**
 * @author kevin
 * @date 2018-10-25 15:14
 */
public interface SysOrgDeptRepository extends StringIdJdbcRepository<SysOrgDept> {
}
