package com.hk.pms.repository.jdbc;


import com.hk.core.data.jdbc.repository.StringIdJdbcRepository;
import com.hk.pms.domain.SysPermission;

/**
 * @author kevin
 * @date 2018-04-12 16:39
 */
public interface SysPermissionRepository extends StringIdJdbcRepository<SysPermission> {
}
