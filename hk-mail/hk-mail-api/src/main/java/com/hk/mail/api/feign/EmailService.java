package com.hk.mail.api.feign;

/**
 * @author huangkai
 * @date 2019-01-08 17:33
 */
interface EmailService {

    String SERVICE_NAME = "hk-email-web";

    String CONTEXT_PATH = "/";
}
