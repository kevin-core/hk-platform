package com.hk.pms.repository.jdbc.impl;

import com.hk.core.data.jdbc.JdbcDaoSupport;
import com.hk.pms.repository.jdbc.custom.CustomSysConfigRepository;

/**
 * @author kevin
 * @date 2018-09-20 15:36
 */
public class SysConfigRepositoryImpl extends JdbcDaoSupport implements CustomSysConfigRepository {

}
